#!/bin/bash
set -e

cd website
yarn install
yarn build
cd ..
cp -R "website/build/site/." "build"