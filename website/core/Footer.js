const React = require('react');

class Footer extends React.Component {

    docUrl(doc) {
        const baseUrl = this.props.config.baseUrl;
        const docsUrl = this.props.config.docsUrl;
        const docsPart = `${docsUrl ? `${docsUrl}/` : ''}`;
        return `${baseUrl}${docsPart}${doc}`;
    }

    render() {
        return (
            <footer className="nav-footer" id="footer">
                <section className="sitemap">
                    <a href={this.props.config.baseUrl} className="nav-home">
                        {this.props.config.footerIcon && (
                            <img
                                src={this.props.config.baseUrl + this.props.config.footerIcon}
                                alt={this.props.config.title}
                                width="66"
                                height="58"
                            />
                        )}
                    </a>
                    <div>
                        <h5>Notes de cours</h5>
                        <a href={this.docUrl('f1')}>
                            Notes de cours
                        </a>
                    </div>
                    <div>
                        <h5>Liens utiles</h5>
                        <a href="https://developer.android.com/" target="_blank" rel="noreferrer noopener">
                            Android Developers
                        </a>
                        <a href="https://material.io/" target="_blank" rel="noreferrer noopener">
                            Material.io
                        </a>
                    </div>
                    <div>
                        <h5>Contribuer</h5>
                        <a href={this.docUrl('contribuing')}>
                            Comment contribuer ?
                        </a>
                        <a href="https://gitlab.com/csfpwmjv-doc/mobile1-doc/issues/new?issuable_template=Bug" target="_blank" rel="noreferrer noopener">
                            Raporter une erreur
                        </a>
                    </div>
                </section>
                <section className="copyright">{this.props.config.copyright}</section>
            </footer>
        );
    }
}

module.exports = Footer;