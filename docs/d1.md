---
id: d1
title: Introduction au Dart
---

<blockquote class="building">

**Page en construction.**

</blockquote>

![Dart](assets/img/d1/dart.png)
Les langages de programmation peuvent être catégorisés de différentes façons, que ce soit par la manière dont ils **gèrent
la mémoire** ou par leur prédispositon à donner des **programmes performants** ou non. Or, peu de critères animent autant 
les débats que le fait qu'un langage soit **typé** ou non.

```cplusplus
//C++ : Fortement Typé
Student readStudent() 
{
    Student student;
    /* ... */
    return student;
}
```

```javascript
//JavaScript : Faiblement typé.
function readStudent() {
    let student = {};
    /* ... */
    return student;                    
}
```

Il est généralement considéré plus simple d'utiliser un langage **fortement typé**, car ils éliminent une grande quantité
de problèmes potentiels dès la compilation. Malgré cela, les langages faiblement typés restent très populaires, le 
*JavaScript* [^1] étant même le langage le plus utilisé dans le monde. 

Pour les habitués des langages fortement typé tel que le *C++* et le *Kotlin*, le *JavaScript* est vu comme imprévisible
et insécure. Naissent alors des alternatives typées tel que [TypeScript](https://www.typescriptlang.org/) 
et [Dart](https://dart.dev/), que nous allons voir durant cette section.

## Raisons pour un autre langage

Pourquoi s'encombrer d'un autre langage ? Encore une fois, tel que mentionné lors de la présentation du [Kotlin](k1.md),
ces nouveaux langages possèdent des fonctionalités fort intéressantes améliorant notre productivité. Dans le cas de 
*Dart*, ce dernier nous sera utile dans la prochaine section sur [le multiplateforme](p1.md). Dart a aussi l'avantage
de pouvoir compiler vers différents *targets*, tel que *Arm32/64*, *X86/64* et même vers *JavaScript*. 

## Dart, TypeScript et le Web

![TypeScript](assets/img/d1/typescript.svg)
L'objectif de *Dart*, à la base, était d'être un 
*[drop-in-replacement](https://en.wikipedia.org/wiki/Drop-in_replacement)* pour le *JavaScript*. Les navigateurs 
auraient donc non seulement supporté l'interprétation de code écrit en *JavaScript*, mais aussi de code écrit en *Dart*.
Google a d'ailleurs considéré très sérieusement de le supporter nativement dans son navigateur maison *Google Chrome*. 
L'idée a cependant été abandonner dû à un manque de popularité du langage [^2] : il a été préféré de compiler *Dart* 
vers du code en *JavaScript*.

Tout comme *Dart*, *TypeScript* est un langage compilant vers du code *JavaScript*. Il est cependant beaucoup plus 
populaire pour le développement web dû à son support complet de l'écosystème *JavaScript* déjà existant. À 
l'inverse, le *Dart* est plus populaire pour le développement mobile, principalement dû à la plateforme 
de développement mobile [Flutter](p1.md).

> Le *JavaScript* est considéré de nos jours comme le *ByteCode* du web, la tandance allant vers des langages plus 
> sércuritaires compilant du code *JavaScript* optimisé et fiable.

<blockquote class="good">

Le langage [Ruby](https://www.ruby-lang.org/fr/) a longtemps survécu uniquement grâce 
à [Ruby On Rail](https://rubyonrails.org/), avant de tomber dans l'oubli absolu. Actuellement, *Dart* survit 
uniquement grâce à *Flutter*. 

Espérons simplement que l'histoire ne se répètera pas...

</blockquote>

[^1]: Stack Overflow,
[Most Popular Technologies](https://insights.stackoverflow.com/survey/2018#most-popular-technologies),
*Stack Overflow - 2019*

[^2]: Dart News & Updates,
[Dart for the Entire Web](https://news.dartlang.org/2015/03/dart-for-entire-web.html),
*Google*