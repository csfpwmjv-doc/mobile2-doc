---
id: contribuing
title: Comment contribuer ?
---

Consultez cette section si vous désirez savoir comment contribuer à cette documentation.

## Contributions désirées

La documentation contenue sur ce site est un travail conséquent. Toute contribution est donc acceptée. Cependant, certains besoins 
sont plus pressants que d'autres, tels que :

* La correction des fautes d'orthographe et de grammaire.
* La création de diagrammes pour expliquer des sujets complexes.
* La correction des erreurs dans les exemples de code.

Si vous désirez contribuer, effectuez une demande d'accès au [dépôt officiel](https://gitlab.com/csfpwmjv-doc/mobile2-doc) 
du projet. Une fois accepté, vous pourrez créer une nouvelle branche avec votre modification et une *merge-request* pour fusionner votre
travail dans la branche principale.

## Code de conduite

Dans l'intérêt de favoriser un environnement ouvert et accueillant, nous nous engageons, en tant que contributeurs 
et responsables de ce projet, à faire de la participation à notre projet une expérience agréable pour tous, quel 
que soit le niveau d'expérience. Nous comprenons que des erreurs peuvent survenir (pour diverses raisons telles que
le manque de sommeil ou une simple mauvaise journée) et nous nous engageons à maintenir un comportement professionnel 
à tout moment.

Exemples de comportements qui contribuent à créer un environnement positif :

* Être respectueux des différents points de vue et expériences
* Accepter la critique lorsqu'elle est constructive
* Toujours agir dans le meilleur intérêt du projet

Exemples de comportements non acceptables :

* L'utilisation d'un langage ou d'imagerie non professionnelle
* Trolling, commentaires insultants et attaques personnelles
* Tout autre comportement qui pourrait être considéré comme inapproprié dans un cadre professionnel

Tout contributeur au projet a le droit et la responsabilité de supprimer, modifier ou rejeter
tout commentaire, code, bogue et autres contributions qui ne respectent pas le code de 
conduite.

Les contributeurs qui ne respectent pas le code de conduite de bonne foi peuvent faire face temporairement ou définitivement
à des répercussions telles que l'expulsion du projet ou toute autre conséquence déterminée par les autres membres du projet.